// MIT License
//
// Copyright (c) 2019 Flyt IT AS
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// ==UserScript==
// @name updown.io dashboard - dark theme
// @namespace https://flytit.no/
// @downloadURL https://gitlab.com/FlytIt/updown-dashboard-userscript/raw/master/updown-dashboard-dark.user.js
// @version 1.0.0
// @description Applies dark theme to updown.io dashboard userscript
// @author Magnus Bjerke Vik <magnus@flytit.no>
// @match https://updown.io/checks
// @grant none
// ==/UserScript==

const css = `
body {
  background-color: #111;
}

#checks h3 a {
  color: #d9d9d9;
}

.status.up {
  background-color: #5c4;
  color: #111;
}

.apdex-timeline output {
  background-color: rgba(215, 215, 215, 0.05);
}

.uptime-timeline i.disabled {
  background-color: #666;
}

.apdex-timeline i, .rt-chart i {
  background-color: #84d578;
  border-top-color: #1c920b;
}

.apdex-timeline i.silver {
  border-top-color: #ff7e0f;
  background-color: #ffb97e;
}

.apdex-timeline i.bad {
  border-top-color: #e43;
  background-color: #f6897e;
}
`

const style = document.createElement('style');
style.type = 'text/css';
style.appendChild(document.createTextNode(css));
document.head.appendChild(style);
