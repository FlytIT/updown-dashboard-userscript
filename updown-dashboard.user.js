// MIT License
//
// Copyright (c) 2019 Flyt IT AS
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// ==UserScript==
// @name updown.io dashboard
// @namespace https://flytit.no/
// @downloadURL https://gitlab.com/FlytIt/updown-dashboard-userscript/raw/master/updown-dashboard.user.js
// @version 1.0.0
// @description Transforms the updown.io checks page to a dashboard with a simpler, cleaner look, and with more space for more items
// @author Magnus Bjerke Vik <magnus@flytit.no>
// @match https://updown.io/checks
// @grant none
// ==/UserScript==

const css = `
body {
  background-color: white;
}

header,
footer,
#status,
body > *:last-child,
#checks .filters,
#checks > *:last-child,
#checks li.form.new,
#checks li .details .actions,
#checks li .details .resolution,
#checks li .timelines .label,
#checks li .url .fallback {
  display: none;
}

#checks,
#checks li {
  background-color: rgba(0, 0, 0, 0);
}

#checks .center {
  max-width: 1900px;
}

#checks ul {
  box-shadow: none;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  column-gap: 20px;
  margin: 0;
}

#checks li {
  max-width: 900px;
  border-bottom: 0;
  padding: 0.75em 0.5em;
}

#checks li .timelines .chart {
  width: 100%;
}

#checks li .details {
  flex: 1 10em;
}
`

const style = document.createElement('style');
style.type = 'text/css';
style.appendChild(document.createTextNode(css));
document.head.appendChild(style);
